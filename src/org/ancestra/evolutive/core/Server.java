package org.ancestra.evolutive.core;

import org.ancestra.evolutive.client.Admin;
import org.ancestra.evolutive.client.Player;
import org.ancestra.evolutive.common.Constants;
import org.ancestra.evolutive.exchange.ExchangeClient;
import org.ancestra.evolutive.game.GameServer;
import org.ancestra.evolutive.tool.command.Command;
import org.ancestra.evolutive.tool.command.Parameter;
import org.ancestra.evolutive.tool.plugin.PluginLoader;
import org.ancestra.evolutive.tool.plugin.packet.Packet;
import org.ancestra.evolutive.tool.plugin.packet.PacketParser;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.Map.Entry;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Server {
	
	public static Server config = new Server();
   
	public static final long startTime = System.currentTimeMillis();
	private boolean isRunning;
	
	private GameServer gameServer;
	private ExchangeClient exchangeClient;

    public void initialize() {
		try {
			//initialisation des logs
			Log.initLogs();
			//initialisation des commandes
			this.initializeCommands();
			//initialisation des packets
			try {
				this.initializePlugins();
			} catch(Exception e) { 
				System.out.println(" <> Erreur lors de l'initialisation des plugins : "+e.getMessage());
                e.printStackTrace();
				System.exit(1);
			}
		} catch(Exception e) {
			System.out.println(" <> Config illisible ou champs manquants: "+e.getMessage());
			System.exit(1);
		}
	}
	
	public void initializeCommands() {
		World.data.getPlayerCommands().put("HELP", new Command<Player>("HELP", "Affiche toute les commandes disponible.", null) {
				
			@Override
			public void action(Player player, String[] args) {
				if(!World.data.getPlayerCommands().isEmpty()) {
					StringBuilder commands = new StringBuilder("<b>Liste des commandes disponibles :</b>");
					for(Command<Player> command: World.data.getPlayerCommands().values()) {
						commands.append("\n").append(command.getName()).append((command.getGmLvl() > 0 ? " (GM : " + command.getGmLvl() + ")": ""));
						if(command.getDescription() != null) 
							commands.append(" - ").append(command.getDescription());
					}
					player.sendText(commands.toString().substring(0,
							commands.toString().length()-2));
				}
			}	
		});
			
		Map<String, Command<Console>> consoleCommands = new HashMap<>();
				
		
		
		/**
		 * Commandes console
		 */
		//informations du serveur
		//creation de la commande
		Command<Console> command = new Command<Console>("INFOS", "Affiche les informations du server.", null) {
				
			@Override
			public void action(Console console, String[] args) {
				Console.instance.writeln(Constants.serverInfos());
			}
				
		};
			
		//ajout aux commmandes
		consoleCommands.put("INFOS", command);
		
		
		//creation de la commande
		command = new Command<Console>("EXIT", "Sauvegarde puis ferme le server.", null) {
				
			@Override
			public void action(Console console, String[] args) {
				World.data.saveData(-1);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {}
				System.exit(0);
			}
				
		};
			
		//ajout aux commmandes
		consoleCommands.put("EXIT", command);
		

		//creation de la commande
		command = new Command<Console>("SAVE", "Sauvegarde le server.", null) {
				
			@Override
			public void action(Console console, String[] args) {
				World.data.saveData(-1);
				Console.instance.write(" <> Sauvegarde terminee");
			}
				
		};
			
		//ajout aux commmandes
		consoleCommands.put("SAVE", command);
		

		//creation de la commande
		command = new Command<Console>("PLUGIN", null, "ADD|SHOW") {
			@Override
			public void action(Console t, String[] args) {
				Console.instance.println("Param�tre non indiqu� : ADD, SHOW");
			}
		};
		
		command.addParameter(new Parameter<Console>("SHOW", "Affiche les diff�rents plug-ins actuellement actif.", null) {

			@Override
			public void action(Console t, String[] args) {
				if(World.data.getOtherPlugins().isEmpty()) {
					Console.instance.writeln("Aucun plug-in est actuellement actif.");
					return;
				}
				
				Console.instance.writeln("Liste des plug-ins actif :");
				
				for(Entry<String, PluginLoader> plugin : World.data.getOtherPlugins().entrySet())
					Console.instance.writeln("--> " + plugin.getKey());					
			}
			
		});

		command.addParameter(new Parameter<Console>("ADD", "Permet d'ajout� un plug-in au server.", null) {

			@Override
			public void action(Console console, String[] args) {
				if(args == null) {
					Console.instance.writeln("Aucun argument d�fini. Merci d'indiquer le nom du fichier compil�.d");
					return;
				}	
				
				final String name = args[0].replace(".jar", "");
				
				FilenameFilter filter = new FilenameFilter() {
					@Override
					public boolean accept(File arg0, String arg1) {
						return arg1.equals(name + ".jar");
					}	
				};
				
				File[] files = new File("./plugins/").listFiles(filter);
				
				if(files != null) {		
					for(File file : files) {
						if(file != null) {
							try {
							World.data.getOtherPlugins()
								.put(file.getName(), new PluginLoader(file));
							} catch(Exception e) {
								Console.instance.writeln("Erreur lors de l'execution du fichier en question.");
                                e.printStackTrace();
							}
						}
					}
				} else {
					Console.instance.writeln("Le fichier " + name +".jar n'existe pas !");
				}	
			}	
		});
				
		//ajout aux commmandes
		consoleCommands.put("PLUGIN", command);
		
		//Commande fixe HELP
		command = new Command<Console>("HELP", null, null) {
			
			@Override
			public void action(Console console, String[] args) {
				StringBuilder commands = new StringBuilder("Liste des commandes disponibles : \n");
				for(Command<Console> command: World.data.getConsoleCommands().values()) {
					if(command == null || (command.getDescription() == null && command.getParameters().isEmpty()))
						continue;
					if(!command.getParameters().isEmpty()) {
						for(Entry<String, Parameter<Console>> parameter : command.getParameters().entrySet()) {
							commands.append("-> ").append(command.getName()).append(" ").append(parameter.getKey()).append((parameter.getValue().getDescription() != null ? " - " + parameter.getValue().getDescription() : "")).append("\n");
						}
					} else {
						commands.append("-> ").append(command.getName()).append((command.getDescription() != null ? " - " + command.getDescription() : "")).append("\n");
					}
				}
					
				Console.instance.writeln(commands.toString().substring(0, commands.toString().length() - 2));
			}
			
		};
		
		//ajout aux commmandes
		consoleCommands.put("HELP", command);
		
		//ajout des commandes dans les donnees du serveur
		World.data.getAdminCommands().putAll(Admin.initialize());
		World.data.getConsoleCommands().putAll(consoleCommands);
	}
		
	public void initializePlugins() throws Exception {
		try {
			String path = getPathOfJarFile();
			this.loadPackets(new File(path), true);
		} catch(NullPointerException e) {
			this.loadPacketsIntoTheJar();
		} catch(Exception e) {
			this.loadPacketsIntoTheJar();
	    }
	   
		FileFilter filter = file -> file.getName().endsWith(".jar");
		
		File[] files = new File("./plugins/").listFiles(filter);
		
		if(files != null) {		
			for(File file : files) {
				if(file != null) {
					try {
					World.data.getOtherPlugins()
						.put(file.getName(), new PluginLoader(file));
					} catch(Exception e) {
                        e.printStackTrace();
						throw new Exception(file.getName() + " : " + e.getMessage());
					}
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	private void loadPacketsIntoTheJar() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		String i = "org.ancestra.evolutive.game.packet.";
		String[] packages = {"account", "basic", "channel", "dialog", "enemy", "environement", 
							 "exchange", "fight", "friend", "game", "group", "guild", "house", 
							 "house.kode", "mount", "object", "panel", "spell", "waypoint"};

		for (String packge : packages) {
			for (Class<?> clas : getClasses(i + packge)) {
				Annotation annotation = clas.getAnnotation(Packet.class); 
				if(annotation instanceof Packet) {
					Packet name = (Packet) annotation;
					World.data.getPacketJar().put(name.value(), (PacketParser) clas.newInstance());
				}
			}
		}	
	}
	
	private String getPathOfJarFile() throws Exception {
	    String path = Main.class.getResource(Main.class.getSimpleName() + ".class").getFile();
	    if(ClassLoader.getSystemClassLoader().getResource(path) != null) 
	    	path = ClassLoader.getSystemClassLoader().getResource(path).getFile();
	    File file = new File(path.substring(0, path.lastIndexOf('!')));
	    return new File("").getAbsolutePath()+(new File("").getAbsolutePath().startsWith("/")?"/":"\\")+file.getName();
	}
		
	@SuppressWarnings("deprecation")
	public void loadPackets(File file, boolean who) throws IOException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		if(file == null)
			return;

		try(JarFile jarFile = new JarFile(new File(file.getPath()))) {
			ClassLoader loader = URLClassLoader.newInstance(
			    new URL[] { file.toURI().toURL() },
			    getClass().getClassLoader()
			);
	
			Enumeration<JarEntry> enumeration = jarFile.entries();
	
			while(enumeration.hasMoreElements()) {
				JarEntry jarEntry = enumeration.nextElement();
				if(jarEntry.getName().endsWith(".class") && jarEntry.getName().startsWith("org/ancestra")) {
					Class<?> localClass = loader.loadClass(jarEntry.getName()
							.replaceAll(".class", "").replaceAll("/", "."));
					Annotation annotation = localClass.getAnnotation(Packet.class); 
					if(annotation instanceof Packet) {
						 Packet name = (Packet) annotation;
						 if(name.value() != null)
							 if(!name.value().equals(""))
								 World.data.getPacketJar().put(name.value(), (PacketParser) localClass.newInstance());
					}
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	private Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
	
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
	
		ArrayList<Class> classes = new ArrayList<>();
	
		for (File directory : dirs) 
			classes.addAll(findClasses(directory, packageName));

		return classes.toArray(new Class[classes.size()]);
	}
	
	@SuppressWarnings("rawtypes")
	private List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
	    List<Class> classes = new ArrayList<Class>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    for (File file : files) {
	        if (file.isDirectory()) {
	            assert !file.getName().contains(".");
	            classes.addAll(findClasses(file, packageName + "." + file.getName()));
	        } else if (file.getName().endsWith(".class")) {
	            classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
	        }
	    }
	    return classes;
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public ExchangeClient getExchangeClient() {
		return exchangeClient;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}

	public void setExchangeClient(ExchangeClient exchangeClient) {
		this.exchangeClient = exchangeClient;
	}

    public void setRunning(boolean running){
        this.isRunning = running;
    }
}
