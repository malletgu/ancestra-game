package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

/**
 * Created by guillaume on 19/09/14.
 */
@Config.Sources({"file:./config.properties"})
public interface RatesConfig extends Reloadable {

    @DefaultValue("1")
    @Key("rates.drop")
    public int drop();

    @DefaultValue("1")
    @Key("rates.kamas")
    public int kamas();

    @DefaultValue("1")
    @Key("rates.honor")
    public int honor();

    @DefaultValue("1")
    @Key("rates.xp.pvm")
    public int xpPvm();

    @DefaultValue("1")
    @Key("rates.xp.pvp")
    public int xpPvp();

    @DefaultValue("1")
    @Key("rates.xp.job")
    public int xpJob();

    @DefaultValue("15")
    @Key("rates.averageLevelPvp")
    public int averageLevelPvp();


}
