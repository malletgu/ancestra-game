package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by guillaume on 19/09/14.
 */
@Config.Sources({"file:./config.properties"})
public interface DatabaseConfig extends Config {

	 @DefaultValue("file:///data")
    @Key("database.dataserver")
    public String gameDataserver();
	
    @DefaultValue("127.0.0.1")
    @Key("database.game.ip")
    public String gameDatabaseIp();

    @DefaultValue("3306")
    @Key("database.game.port")
    public int gameDatabasePort();

    @DefaultValue("ae_game")
    @Key("database.game.table_name")
    public String gameDatabaseName();

    @DefaultValue("root")
    @Key("database.game.user.name")
    public String gameDatabaseUser();

    @DefaultValue("")
    @Key("database.game.user.password")
    public String gameDatabasePassword();

    @DefaultValue("127.0.0.1")
    @Key("database.login.ip")
    public String loginDatabaseIp();

    @DefaultValue("3306")
    @Key("database.login.port")
    public int loginDatabasePort();

    @DefaultValue("ae_login")
    @Key("database.login.table_name")
    public String loginDatabaseName();

    @DefaultValue("root")
    @Key("database.login.user.name")
    public String loginDatabaseUser();

    @DefaultValue("")
    @Key("database.login.user.password")
    public String loginDatabasePassword();

}
