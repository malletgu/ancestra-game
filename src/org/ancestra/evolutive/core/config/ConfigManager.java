package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.ConfigFactory;
import org.aeonbits.owner.Reloadable;

/**
 * Created by guillaume on 21/09/14.
 */
public class ConfigManager {

    public final static DatabaseConfig databaseConfig = ConfigFactory.create(DatabaseConfig.class);
    public final static GameConfig gameConfig = ConfigFactory.create(GameConfig.class);
    public final static NetworkConfig networkConfig = ConfigFactory.create(NetworkConfig.class);
    public final static RatesConfig ratesConfig = ConfigFactory.create(RatesConfig.class);
    public final static ServerConfig serverConfig = ConfigFactory.create(ServerConfig.class);

    public static void reloadConfig(Reloadable config){
        config.reload();
    }
}
