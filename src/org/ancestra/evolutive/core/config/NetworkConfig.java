package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by guillaume on 19/09/14.
 */
@Config.Sources({"file:./config.properties"})
public interface NetworkConfig extends Config {


    @DefaultValue("127.0.0.1")
    @Key("network.game.ip")
    public String gameIp();

    @DefaultValue("5555")
    @Key("network.game.port")
    public int gamePort();

    @DefaultValue("127.0.0.1")
    @Key("network.exchange.ip")
    public String exchangeIp();

    @DefaultValue("6666")
    @Key("network.exchange.port")
    public int exchangePort();


}
