package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by guillaume on 19/09/14.
 */
@Config.Sources({"file:./config.properties"})
public interface ServerConfig extends Config {

    @DefaultValue("1")
    @Key("server.id")
    public int serverId();

    @DefaultValue("jiva")
    @Key("server.key")
    public String serverKey();

    @DefaultValue("true")
    @Key("server.allowNameGenerating")
    public boolean nameGenerator();

    @DefaultValue("true")
    @Key("server.regenLifeWhenOffline")
    public boolean regenLifeWhenOffline();

    @DefaultValue("3600000")
    @Key("server.max_idle_time")
    public int maxIdleTime();

    @DefaultValue("1800000")
    @Key("server.save_time")
    public int saveTime();

    @DefaultValue("60000")
    @Key("server.flood_time")
    public int floodTime();

    @DefaultValue("60000")
    @Key("server.liveActionDelay")
    public int liveActionDelay();

    @DefaultValue("180000")
    @Key("server.mob.move_delay")
    public int moveMobDelay();

    @DefaultValue("18000000")
    @Key("server.mob.recreate_delay")
    public int recreateMobDelay();

    @DefaultValue("500")
    @Key("server.player_limit")
    public int playerLimit();

    @DefaultValue("Bienvenue sur le serveur Ancestra Evolutive")
    @Key("server.welcome_message")
    public String welcomeMessage();

    @DefaultValue("000000")
    @Key("server.welcome_message_color")
    public String welcomeColor();

    @Config.DefaultValue("1")
    @Config.Key("server.ia_thread_quantity")
    public int iaThreadQuantity();
}
