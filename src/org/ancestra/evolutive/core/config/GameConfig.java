package org.ancestra.evolutive.core.config;

import org.aeonbits.owner.Config;

import java.util.ArrayList;

/**
 * Created by guillaume on 19/09/14.
 */
@Config.Sources({"file:./config.properties"})
public interface GameConfig extends Config{

    @DefaultValue("5")
    @Key("game.restriction.account.max_player")
    public int maxPlayerPerAccount();

    @DefaultValue("true")
    @Key("game.restriction.multi_account")
    public boolean multiAccount();

    @DefaultValue("true")
    @Key("game.restriction.mulePvp")
    public boolean mulePvp();

    @DefaultValue("1")
    @Key("game.start.level")
    public int startLevel();

    @DefaultValue("0")
    @Key("game.start.kamas")
    public int startKamas();

    @DefaultValue("-1")
    @Key("game.start.map")
    public int startMap();

    @DefaultValue("-1")
    @Key("game.start.cell")
    public int startCell();


    @DefaultValue("false")
    @Key("game.start.zaaps")
    public boolean allZaaps();

    @DefaultValue("600000")
    @Key("game.arena.timer")
    public int arenaTimer();

    @Separator(",")
    @DefaultValue("10134,10132,10131,10133,10137,10135,10136,10138")
    @Key("game.arena.maps")
    public ArrayList<Integer> arenaMaps();

    @Separator(",")
    @DefaultValue("10134,10132,10131,10133,10137,10135,10136,10138")
    @Key("game.maps.marchand")
    public ArrayList<Integer> marchandMaps();

    @Separator(",")
    @DefaultValue("10134,10132,10131,10133,10137,10135,10136,10138")
    @Key("game.maps.collector")
    public ArrayList<Integer> collectorMaps();

    @Separator(",")
    @DefaultValue("")
    @Key("game.hdv.item_forbidden")
    public ArrayList<Integer> notInHdv();

}
