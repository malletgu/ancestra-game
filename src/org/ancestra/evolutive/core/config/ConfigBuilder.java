package org.ancestra.evolutive.core.config;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.aeonbits.owner.Config;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

/**
 * Created by guillaume on 22/09/14.
 */
public class ConfigBuilder {

    public static void buildConfigFile(File fichier){
        final StringBuilder str = new StringBuilder();
        for(Field field : ConfigManager.class.getFields()){
            try {
                Class<? extends Config> classe = (Class<? extends Config>) field.getType();
                str.append(generateConfig(classe));
            } catch (Exception ignored){ }
        }
        try {
            if(!fichier.exists()){
                final BufferedWriter out = new BufferedWriter(new FileWriter(fichier));
                out.write(str.toString());
                out.flush();
                out.close();
            }
        } catch (IOException e) {
            final Logger logger = (Logger)LoggerFactory.getLogger(ConfigBuilder.class);
            logger.error("Impossible de creer le fichier de configuration par defaut");
        }

    }

    private static String generateConfig(Class<?> classe){
        final StringBuilder str = new StringBuilder();
        str.append(header(classe));
        for (Method method : classe.getDeclaredMethods()){
            str.append(methodDescriptor(method));
            str.append("\n");
        }
        return str.toString();
    }

    private static String header(Class<?> classe){
        return "#Fichier de configuration de " + classe.getSimpleName() + "\n";
    }

    private static String methodDescriptor(Method method){
        final StringBuilder str = new StringBuilder();
        String key = "",defaultValue = "",separator = "";
        for(Annotation annotation : method.getDeclaredAnnotations()){
            if(annotation.annotationType() == Config.Key.class){
                key = ((Config.Key)annotation).value();
                continue;
            }
            if(annotation.annotationType() == Config.DefaultValue.class){
                defaultValue = ((Config.DefaultValue)annotation).value();
                continue;
            }
            if(annotation.annotationType() == Config.Separator.class){
                separator = ((Config.Separator)annotation).value();
                continue;
            }
        }
        if(!separator.isEmpty())
            str.append('#').append("Le separateur est ").append(separator).append("\n");
        if(!key.isEmpty())
            str.append("#").append(key).append("=").append(defaultValue).append("\n");
        return str.toString();
    }
}
