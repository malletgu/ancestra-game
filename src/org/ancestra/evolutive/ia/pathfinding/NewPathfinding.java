package org.ancestra.evolutive.ia.pathfinding;

import com.mysql.fabric.xmlrpc.base.Array;
import org.ancestra.evolutive.entity.creature.Creature;
import org.ancestra.evolutive.entity.creature.Fightable;
import org.ancestra.evolutive.map.Case;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by guillaume on 26/09/14.
 */
public class NewPathfinding {

    ArrayList<Case> generateMinimalPathInFight(Case cell1,Case cell2,int maxDistance){
        ArrayList<Case> itineraire = new ArrayList<>();
        Case currentCell = cell1;
        for(int i = 0; i != maxDistance && i < 100;i++){
            ArrayList<Case> possibilities = PathFindingUtil.getDirectNeighboursWalkable(currentCell, true);
            removeUsedCell(possibilities);
            int min = 500;
            Case caseMin = null;
            for(Case cell : possibilities){
                if(!itineraire.contains(cell) && PathFindingUtil.distanceBeetween(cell,cell2)<min){
                    min = PathFindingUtil.distanceBeetween(cell,cell2);
                    caseMin = cell;
                }
            }
            itineraire.add(caseMin);
        }
        return itineraire;
    }

    private void removeUsedCell(ArrayList<Case> cells){
        Iterator<Case> it = cells.iterator();
        while(it.hasNext()) {
            Case cell = it.next();
            if (!cell.isFree()) {
                for (Creature creature : cell.getCreature()) {
                    if (!((Fightable) creature).getFighter().isHide())
                        cells.remove(cell);
                }
            }
        }
    }


}
