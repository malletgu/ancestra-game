package org.ancestra.evolutive.ia.pathfinding;


import com.mysql.fabric.xmlrpc.base.Array;
import org.ancestra.evolutive.map.Case;
import org.ancestra.evolutive.map.Maps;

import java.util.ArrayList;

/**
 * Created by guillaume on 26/09/14.
 */
public class PathFindingUtil {



    static int distanceBeetween(Case cell1,Case cell2){
        int distance = 0;
        distance += Math.abs(cell1.getX()-cell2.getY());
        distance += Math.abs(cell1.getY()-cell2.getY());
        return distance;
    }

    static ArrayList<Case> getDirectNeighboursWalkable(Case cell){
        return getDirectNeighboursWalkable(cell, true);
    }

    /**
     * Get a list of cell in contact with the specified one
     * @param cell middle
     * @param allowDiagonal if diagonal can be use (false for Fight)
     * @return list of direct neighbours
     */
    static ArrayList<Case> getDirectNeighboursWalkable(Case cell,boolean allowDiagonal){
        ArrayList<Case> result = new ArrayList<>();
        addIfExistAndWalkable(result, cell.getMap(), cell.getId() + cell.getMap().getWidth());
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() + cell.getMap().getWidth()-1);
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() - cell.getMap().getWidth());
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() - cell.getMap().getWidth()+1);
        if(!allowDiagonal) return result;
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() + 1);
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() + cell.getMap().getWidth()*2-1);
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() - 1);
        addIfExistAndWalkable(result,cell.getMap(),cell.getId() - (cell.getMap().getWidth()*2-1));
        return result;
    }

    private static void addIfExistAndWalkable(ArrayList<Case> cells,Maps map, int cellId){
        Case cell = map.getCases().getOrDefault(cellId,null);
        if(cell != null && cell.isWalkable())
            cells.add(cell);
    }

}
