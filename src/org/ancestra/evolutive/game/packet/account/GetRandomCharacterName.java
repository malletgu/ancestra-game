package org.ancestra.evolutive.game.packet.account;

import org.ancestra.evolutive.common.SocketManager;
import org.ancestra.evolutive.core.Console;
import org.ancestra.evolutive.core.Server;
import org.ancestra.evolutive.core.config.ConfigManager;
import org.ancestra.evolutive.game.GameClient;
import org.ancestra.evolutive.tool.plugin.packet.Packet;
import org.ancestra.evolutive.tool.plugin.packet.PacketParser;

@Packet("AP")
public class GetRandomCharacterName implements PacketParser {

	@Override
	public void parse(GameClient client, String packet) {
		if(ConfigManager.serverConfig.nameGenerator())
			client.send("APK" + generateName());
		else
			client.send("APE2");
	}

    private String generateName(){
        String chars = "abcdefghijklmnopqrstuvwxyz"; // Tu supprimes les lettres dont tu ne veux pas
        String pass = "";
        for(int x=0;x<5;x++){
            int i = (int)Math.floor(Math.random() * 26); // Si tu supprimes des lettres tu diminues ce nb
            pass += chars.charAt(i);
        }
        return pass;
    }
}