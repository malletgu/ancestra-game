package org.ancestra.evolutive.new_database;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.ancestra.evolutive.core.config.ConfigManager;
import org.ancestra.evolutive.new_database.data.spells.XmlSpell;
import org.ancestra.evolutive.new_database.data.spells.XmlSpells;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class DataManager {
	private static Logger logger = (Logger) LoggerFactory.getLogger(DataManager.class);
	public static Map<Integer, XmlSpell> spells;
	
	public static void loadData() {
		try {
			JAXBContext context = JAXBContext.newInstance(XmlSpells.class);
			Unmarshaller u = context.createUnmarshaller();
			URL dataserver = new URL(ConfigManager.databaseConfig.gameDataserver());
			boolean isLocal = dataserver.getProtocol().equalsIgnoreCase("file");
			
			Map<Class<?>, List<File>> dataFiles = isLocal?loadLocalDataDir(Paths.get(".", dataserver.getPath())):loadDistantDataFile(dataserver);
			if(dataFiles == null) {
				logger.error("Error while loading dataserver data");
				System.exit(1);
			}
			//Load Spells
			spells = loadDataFiles(u, dataFiles.get(XmlSpell.class), XmlSpells.class, XmlSpells::getSpells, Collectors.toMap(XmlSpell::getId, s -> s));
			spells.values().parallelStream().forEach(XmlSpell::linkLevels);
			logger.info(spells.size()+" spells loaded!");
			
		} catch (JAXBException e) {
			logger.error("Unable to load static data: "+e.getClass().getSimpleName()+" "+e.getMessage(), e);
			System.exit(1);
		} catch (MalformedURLException e) {
			logger.error("Invalid URL for dataserver config");
			System.exit(1);
		} catch(IOException e) {
			logger.error("Unable to load static data: "+e.getClass().getSimpleName()+" "+e.getMessage(), e);
			System.exit(1);
		};
		
	}
	
	private static Map<Class<?>, List<File>> loadDistantDataFile(URL dataserver) {
		// TODO loadDistantDataFile
		return null;
	}

	private static Map<Class<?>, List<File>> loadLocalDataDir(Path path) throws IOException {
		HashMap<Class<?>, List<File>> dataFiles = new HashMap<>();
		dataFiles.put(XmlSpell.class, new ArrayList<>());
		//dataFiles.put(Map.class, new ArrayList<>());
		//etc
		
		Files.walk(path.normalize()).filter(p-> Files.isRegularFile(p) && p.getFileName().toString().endsWith(".xml")).forEach(p -> {
			switch(p.getName(1).toString()) {
			case "spells": dataFiles.get(XmlSpell.class).add(p.toFile());return;
			default:logger.warn("Unknown data type '"+p.getName(2)+"', ignored");return;
			}
		});
		return dataFiles;
	}

	private static <L, T, R> R loadDataFiles(Unmarshaller u, List<File> files, Class<L> dataType, Function<L, List<T>> mapper, Collector<T,?,R> collector) throws JAXBException {
		return files.stream().<L>map(f -> {
			try {
				return u.unmarshal(new StreamSource(f), dataType).getValue();
			}catch(JAXBException e) { return null; }
		}).map(mapper).flatMap(list -> list.stream()).collect(collector);
	}
	
}
