
package org.ancestra.evolutive.new_database.data.spells;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour xmlEffect complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="xmlEffect">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="param1" use="required" type="{http://www.w3.org/2001/XMLSchema}short" />
 *       &lt;attribute name="param2" use="required" type="{http://www.w3.org/2001/XMLSchema}short" />
 *       &lt;attribute name="param3" use="required" type="{http://www.w3.org/2001/XMLSchema}short" />
 *       &lt;attribute name="percentProba" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;minInclusive value="0"/>
 *             &lt;maxInclusive value="100"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="diceCount" use="required" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *       &lt;attribute name="diceSize" use="required" type="{http://www.w3.org/2001/XMLSchema}short" />
 *       &lt;attribute name="diceConstant" use="required" type="{http://www.w3.org/2001/XMLSchema}short" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlEffect")
@XmlSeeAlso({
    XmlSpellEffect.class
})
public class XmlEffect {

    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "param1", required = true)
    protected short param1;
    @XmlAttribute(name = "param2", required = true)
    protected short param2;
    @XmlAttribute(name = "param3", required = true)
    protected short param3;
    @XmlAttribute(name = "percentProba", required = true)
    protected byte percentProba;
    @XmlAttribute(name = "diceCount", required = true)
    protected byte diceCount;
    @XmlAttribute(name = "diceSize", required = true)
    protected byte diceSize;
    @XmlAttribute(name = "diceConstant", required = true)
    protected short diceConstant;

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propri�t� param1.
     * 
     */
    public short getParam1() {
        return param1;
    }

    /**
     * D�finit la valeur de la propri�t� param1.
     * 
     */
    public void setParam1(short value) {
        this.param1 = value;
    }

    /**
     * Obtient la valeur de la propri�t� param2.
     * 
     */
    public short getParam2() {
        return param2;
    }

    /**
     * D�finit la valeur de la propri�t� param2.
     * 
     */
    public void setParam2(short value) {
        this.param2 = value;
    }

    /**
     * Obtient la valeur de la propri�t� param3.
     * 
     */
    public short getParam3() {
        return param3;
    }

    /**
     * D�finit la valeur de la propri�t� param3.
     * 
     */
    public void setParam3(short value) {
        this.param3 = value;
    }

    /**
     * Obtient la valeur de la propri�t� percentProba.
     * 
     */
    public byte getPercentProba() {
        return percentProba;
    }

    /**
     * D�finit la valeur de la propri�t� percentProba.
     * 
     */
    public void setPercentProba(byte value) {
        this.percentProba = value;
    }

    /**
     * Obtient la valeur de la propri�t� diceCount.
     * 
     */
    public byte getDiceCount() {
        return diceCount;
    }

    /**
     * D�finit la valeur de la propri�t� diceCount.
     * 
     */
    public void setDiceCount(byte value) {
        this.diceCount = value;
    }

    /**
     * Obtient la valeur de la propri�t� diceSize.
     * 
     */
    public byte getDiceSize() {
        return diceSize;
    }

    /**
     * D�finit la valeur de la propri�t� diceSize.
     * 
     */
    public void setDiceSize(byte value) {
        this.diceSize = value;
    }

    /**
     * Obtient la valeur de la propri�t� diceConstant.
     * 
     */
    public short getDiceConstant() {
        return diceConstant;
    }

    /**
     * D�finit la valeur de la propri�t� diceConstant.
     * 
     */
    public void setDiceConstant(short value) {
        this.diceConstant = value;
    }

}
