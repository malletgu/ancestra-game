
package org.ancestra.evolutive.new_database.data.spells;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.ancestra.evolutive.jaxb.adapters.AoETypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlAreaOfEffect", propOrder = {
    "type",
    "sizeParams"
})
public class XmlAreaOfEffect {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(AoETypeAdapter.class)
    protected char type;
    @XmlList
    @XmlElement(type = Byte.class)
    protected List<Byte> sizeParams;

    public char getType() {
        return type;
    }

    public List<Byte> getSizeParams() {
        if (sizeParams == null) {
            sizeParams = new ArrayList<Byte>();
        }
        return this.sizeParams;
    }

}
