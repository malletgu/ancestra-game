
package org.ancestra.evolutive.new_database.data.spells;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour xmlSpellLevel complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="xmlSpellLevel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="normalEffects" type="{}xmlSpellEffect" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="criticalEffects" type="{}xmlSpellEffect" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="apCost" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="minRange" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="maxRange" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="criticalHit" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="criticalFailure" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="lineOnlyCastable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="needsLineOfSight" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="freeCellOnlyCastable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="rangeBoostable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="maxLaunchByTurn" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="maxLaunchByPlayer" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="cooldown" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="requiredStates" type="{http://www.w3.org/2001/XMLSchema}short" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="forbiddenStates" type="{http://www.w3.org/2001/XMLSchema}short" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="failureEndsTurn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *       &lt;attribute name="level" use="required" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlSpellLevel", propOrder = {
    "normalEffects",
    "criticalEffects",
    "apCost",
    "minRange",
    "maxRange",
    "criticalHit",
    "criticalFailure",
    "lineOnlyCastable",
    "needsLineOfSight",
    "freeCellOnlyCastable",
    "rangeBoostable",
    "maxLaunchByTurn",
    "maxLaunchByPlayer",
    "cooldown",
    "requiredStates",
    "forbiddenStates",
    "failureEndsTurn"
})
public class XmlSpellLevel {
	protected transient XmlSpell spell;
    protected List<XmlSpellEffect> normalEffects;
    protected List<XmlSpellEffect> criticalEffects;
    protected byte apCost;
    protected byte minRange;
    protected byte maxRange;
    protected byte criticalHit;
    protected byte criticalFailure;
    protected boolean lineOnlyCastable;
    protected boolean needsLineOfSight;
    protected boolean freeCellOnlyCastable;
    protected boolean rangeBoostable;
    protected byte maxLaunchByTurn;
    protected byte maxLaunchByPlayer;
    protected byte cooldown;
    @XmlElement(type = Short.class)
    protected List<Short> requiredStates;
    @XmlElement(type = Short.class)
    protected List<Short> forbiddenStates;
    protected boolean failureEndsTurn;
    @XmlAttribute(name = "level", required = true)
    protected byte level;

    /**
     * Gets the value of the normalEffects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the normalEffects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNormalEffects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlSpellEffect }
     * 
     * 
     */
    public List<XmlSpellEffect> getNormalEffects() {
        if (normalEffects == null) {
            normalEffects = new ArrayList<XmlSpellEffect>();
        }
        return this.normalEffects;
    }

    /**
     * Gets the value of the criticalEffects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the criticalEffects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriticalEffects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlSpellEffect }
     * 
     * 
     */
    public List<XmlSpellEffect> getCriticalEffects() {
        if (criticalEffects == null) {
            criticalEffects = new ArrayList<XmlSpellEffect>();
        }
        return this.criticalEffects;
    }

    /**
     * Obtient la valeur de la propri�t� apCost.
     * 
     */
    public byte getApCost() {
        return apCost;
    }

    /**
     * D�finit la valeur de la propri�t� apCost.
     * 
     */
    public void setApCost(byte value) {
        this.apCost = value;
    }

    /**
     * Obtient la valeur de la propri�t� minRange.
     * 
     */
    public byte getMinRange() {
        return minRange;
    }

    /**
     * D�finit la valeur de la propri�t� minRange.
     * 
     */
    public void setMinRange(byte value) {
        this.minRange = value;
    }

    /**
     * Obtient la valeur de la propri�t� maxRange.
     * 
     */
    public byte getMaxRange() {
        return maxRange;
    }

    /**
     * D�finit la valeur de la propri�t� maxRange.
     * 
     */
    public void setMaxRange(byte value) {
        this.maxRange = value;
    }

    /**
     * Obtient la valeur de la propri�t� criticalHit.
     * 
     */
    public byte getCriticalHit() {
        return criticalHit;
    }

    /**
     * D�finit la valeur de la propri�t� criticalHit.
     * 
     */
    public void setCriticalHit(byte value) {
        this.criticalHit = value;
    }

    /**
     * Obtient la valeur de la propri�t� criticalFailure.
     * 
     */
    public byte getCriticalFailure() {
        return criticalFailure;
    }

    /**
     * D�finit la valeur de la propri�t� criticalFailure.
     * 
     */
    public void setCriticalFailure(byte value) {
        this.criticalFailure = value;
    }

    /**
     * Obtient la valeur de la propri�t� lineOnlyCastable.
     * 
     */
    public boolean isLineOnlyCastable() {
        return lineOnlyCastable;
    }

    /**
     * D�finit la valeur de la propri�t� lineOnlyCastable.
     * 
     */
    public void setLineOnlyCastable(boolean value) {
        this.lineOnlyCastable = value;
    }

    /**
     * Obtient la valeur de la propri�t� needsLineOfSight.
     * 
     */
    public boolean isNeedsLineOfSight() {
        return needsLineOfSight;
    }

    /**
     * D�finit la valeur de la propri�t� needsLineOfSight.
     * 
     */
    public void setNeedsLineOfSight(boolean value) {
        this.needsLineOfSight = value;
    }

    /**
     * Obtient la valeur de la propri�t� freeCellOnlyCastable.
     * 
     */
    public boolean isFreeCellOnlyCastable() {
        return freeCellOnlyCastable;
    }

    /**
     * D�finit la valeur de la propri�t� freeCellOnlyCastable.
     * 
     */
    public void setFreeCellOnlyCastable(boolean value) {
        this.freeCellOnlyCastable = value;
    }

    /**
     * Obtient la valeur de la propri�t� rangeBoostable.
     * 
     */
    public boolean isRangeBoostable() {
        return rangeBoostable;
    }

    /**
     * D�finit la valeur de la propri�t� rangeBoostable.
     * 
     */
    public void setRangeBoostable(boolean value) {
        this.rangeBoostable = value;
    }

    /**
     * Obtient la valeur de la propri�t� maxLaunchByTurn.
     * 
     */
    public byte getMaxLaunchByTurn() {
        return maxLaunchByTurn;
    }

    /**
     * D�finit la valeur de la propri�t� maxLaunchByTurn.
     * 
     */
    public void setMaxLaunchByTurn(byte value) {
        this.maxLaunchByTurn = value;
    }

    /**
     * Obtient la valeur de la propri�t� maxLaunchByPlayer.
     * 
     */
    public byte getMaxLaunchByPlayer() {
        return maxLaunchByPlayer;
    }

    /**
     * D�finit la valeur de la propri�t� maxLaunchByPlayer.
     * 
     */
    public void setMaxLaunchByPlayer(byte value) {
        this.maxLaunchByPlayer = value;
    }

    /**
     * Obtient la valeur de la propri�t� cooldown.
     * 
     */
    public byte getCooldown() {
        return cooldown;
    }

    /**
     * D�finit la valeur de la propri�t� cooldown.
     * 
     */
    public void setCooldown(byte value) {
        this.cooldown = value;
    }

    /**
     * Gets the value of the requiredStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requiredStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequiredStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Short }
     * 
     * 
     */
    public List<Short> getRequiredStates() {
        if (requiredStates == null) {
            requiredStates = new ArrayList<Short>();
        }
        return this.requiredStates;
    }

    /**
     * Gets the value of the forbiddenStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forbiddenStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForbiddenStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Short }
     * 
     * 
     */
    public List<Short> getForbiddenStates() {
        if (forbiddenStates == null) {
            forbiddenStates = new ArrayList<Short>();
        }
        return this.forbiddenStates;
    }

    /**
     * Obtient la valeur de la propri�t� failureEndsTurn.
     * 
     */
    public boolean isFailureEndsTurn() {
        return failureEndsTurn;
    }

    /**
     * D�finit la valeur de la propri�t� failureEndsTurn.
     * 
     */
    public void setFailureEndsTurn(boolean value) {
        this.failureEndsTurn = value;
    }

    /**
     * Obtient la valeur de la propri�t� level.
     * 
     */
    public byte getLevel() {
        return level;
    }

    /**
     * D�finit la valeur de la propri�t� level.
     * 
     */
    public void setLevel(byte value) {
        this.level = value;
    }
    
    void setSpell(XmlSpell spell) {
    	this.spell = spell;
    }
    

}
