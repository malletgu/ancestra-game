
package org.ancestra.evolutive.new_database.data.spells;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour xmlSpell complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="xmlSpell">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="levels" type="{}xmlSpellLevel" maxOccurs="6"/>
 *         &lt;element name="spriteId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="spriteInfos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlSpell", propOrder = {
    "id",
    "levels",
    "spriteId",
    "spriteInfos"
})
public class XmlSpell {

    protected int id;
    @XmlElement(required = true)
    protected List<XmlSpellLevel> levels;
    protected int spriteId;
    @XmlElement(required = true)
    protected String spriteInfos;

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the levels property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the levels property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLevels().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlSpellLevel }
     * 
     * 
     */
    public List<XmlSpellLevel> getLevels() {
        if (levels == null) {
            levels = new ArrayList<>();
        }
        return this.levels;
    }

    /**
     * Obtient la valeur de la propri�t� spriteId.
     * 
     */
    public int getSpriteId() {
        return spriteId;
    }

    /**
     * D�finit la valeur de la propri�t� spriteId.
     * 
     */
    public void setSpriteId(int value) {
        this.spriteId = value;
    }

    /**
     * Obtient la valeur de la propri�t� spriteInfos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpriteInfos() {
        return spriteInfos;
    }

    /**
     * D�finit la valeur de la propri�t� spriteInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpriteInfos(String value) {
        this.spriteInfos = value;
    }
    
	public void linkLevels() {
		levels.forEach(l -> l.setSpell(this));
	}
}
