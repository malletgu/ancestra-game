
package org.ancestra.evolutive.new_database.data.spells;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour xmlSpellEffect complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="xmlSpellEffect">
 *   &lt;complexContent>
 *     &lt;extension base="{}xmlEffect">
 *       &lt;sequence>
 *         &lt;element name="area" type="{}xmlAreaOfEffect"/>
 *       &lt;/sequence>
 *       &lt;attribute name="duration" use="required" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *       &lt;attribute name="ignoreCaster" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ignorePlayers" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ignoreSummon" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ignoreAllies" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ignoreEnnemy" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlSpellEffect", propOrder = {
    "area"
})
public class XmlSpellEffect
    extends XmlEffect
{

    @XmlElement(required = true)
    protected XmlAreaOfEffect area;
    @XmlAttribute(name = "duration", required = true)
    protected byte duration;
    @XmlAttribute(name = "ignoreCaster", required = true)
    protected boolean ignoreCaster;
    @XmlAttribute(name = "ignorePlayers", required = true)
    protected boolean ignorePlayers;
    @XmlAttribute(name = "ignoreSummon", required = true)
    protected boolean ignoreSummon;
    @XmlAttribute(name = "ignoreAllies", required = true)
    protected boolean ignoreAllies;
    @XmlAttribute(name = "ignoreEnnemy", required = true)
    protected boolean ignoreEnnemy;


    public XmlAreaOfEffect getArea() {
        return area;
    }

    public byte getDuration() {
        return duration;
    }

    public boolean isIgnoreCaster() {
        return ignoreCaster;
    }

    public boolean isIgnorePlayers() {
        return ignorePlayers;
    }

    public boolean isIgnoreSummon() {
        return ignoreSummon;
    }

    public boolean isIgnoreAllies() {
        return ignoreAllies;
    }

    public boolean isIgnoreEnnemy() {
        return ignoreEnnemy;
    }

	public void apply(Object fight, Object caster, short cellId) {
		//Do nothing by default
	}

}
