package org.ancestra.evolutive.enums;

/**
 * Created by guillaume on 22/09/14.
 */
public enum ServerState {
    OFFLINE(0),
    ONLINE(1),
    SAVING(2);

    private final int id;

    private ServerState(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public String toString(){
        return Integer.toString(id);
    }
}
