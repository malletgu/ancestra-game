package org.ancestra.evolutive.newfight;

import java.util.List;

import org.ancestra.evolutive.new_database.data.spells.XmlEffect;

public interface IFight {
	
	public List<IFighter> getFighters();
	public List<IFighter> getAffectedFighters(XmlEffect effect, short cellId);
	public BuffManager getBuffManager();
	
	public void teleportFighter(IFighter fighter, short cellId);
	public void setFighterLife(IFighter t, int i);

}
