package org.ancestra.evolutive.newfight;

public class CastInfos {
	public final int effectId;
	public int effectValue; 
	public IFighter caster;//Fighter
	public IFighter target;//Fighter
	
	public CastInfos(int effectId, int effectValue, IFighter caster, IFighter target) {
		this.effectId = effectId;
		this.effectValue = effectValue;
		this.caster = caster;
		this.target = target;
	}
}
