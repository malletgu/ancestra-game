package org.ancestra.evolutive.newfight;

import org.ancestra.evolutive.new_database.data.spells.XmlSpellEffect;

@FunctionalInterface
public interface ISpellEffect {
	
	public default void onFightStart(IFight fight) {}

	public void apply(IFight fight, IFighter caster, XmlSpellEffect effect, short cellId);
	
	public default void onFightEnd(IFight fight) {}
}
