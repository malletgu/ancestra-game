package org.ancestra.evolutive.newfight;


public interface IFighter {

	public short getCellId();
	public boolean isAlive();
	
}
