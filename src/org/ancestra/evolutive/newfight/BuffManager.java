package org.ancestra.evolutive.newfight;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class BuffManager {
	private final List<Buff> buffs = new ArrayList<>();
	
	public void addBuff(Buff buff) {
		buffs.add(buff);
	}
	
	/**
	 * Do what we have to do when a fighter die:
	 * - Removes all buff casted by fighter
	 * @param fighter who died
	 */
	public void onFighterDie(IFighter fighter) {
		Iterator<Buff> it = buffs.iterator();
		while(it.hasNext()) {
			if(it.next().caster == fighter) {
				it.remove();
			}
		}
	}
	
	public void onTurnStarts(IFighter fighter) {
		Iterator<Buff> it = buffs.iterator();
		while(it.hasNext() && fighter.isAlive()) {
			if(it.next().onTurnStart(fighter)) {
				it.remove();
			}
		}
	}
	
	public void onHit(CastInfos infos) {
		Iterator<Buff> it = buffs.iterator();
		while(it.hasNext() && infos.target.isAlive()) {
			if(it.next().onHit(infos))break;
		}
	}
	
	public void onTurnEnds(IFighter fighter) {
		Iterator<Buff> it = buffs.iterator();
		while(it.hasNext() && fighter.isAlive()) {
			it.next().onTurnEnd(fighter);
		}
	}
}
