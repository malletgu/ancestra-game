package org.ancestra.evolutive.newfight;

public class Buff {
	protected final IFighter caster;
	protected final IFighter target;
	protected byte duration;
	
	public Buff(IFighter caster, IFighter target, byte duration) {
		super();
		this.caster = caster;
		this.target = target;
		this.duration = duration;
	}
	
	protected void applyOnTurnStart() {
		//Do nothing by default
	}
	
	protected boolean applyOnHit(CastInfos castInfo) {
		//Do nothing by default, do not stop the buff chain
		return false;
	}
	
	protected void applyOnTurnEnd() {
		//Do nothing by default
	}
	
	/**
	 * Call on start of every fighter turn
	 * @param player
	 * @return true if the buff is finished
	 */
	public final boolean onTurnStart(IFighter player) {
		if(player == target) {
			applyOnTurnStart();
		}
		if(player == caster) {
			duration--;
			if(duration == 0) return true;
		}
		return false;
	}
	
	/**
	 * Apply "on hit" buffs
	 * @param castInfo informations about the casted spell
	 * @return true if the buff chain must be stopped
	 */
	public final boolean onHit(CastInfos castInfo) {
		if(castInfo.target != target) return false;
		return applyOnHit(castInfo);
	}
	
	public final void onTurnEnd(IFighter player) {
		if(player != target) return;
		applyOnTurnEnd();
	}
}
