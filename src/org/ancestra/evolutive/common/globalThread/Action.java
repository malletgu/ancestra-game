package org.ancestra.evolutive.common.globalThread;

public interface Action {
	public void applyAction();
}