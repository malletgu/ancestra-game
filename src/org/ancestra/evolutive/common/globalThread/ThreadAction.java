package org.ancestra.evolutive.common.globalThread;

public interface ThreadAction extends Action {
	public void applyAction();
    public boolean actionNeeded();
}