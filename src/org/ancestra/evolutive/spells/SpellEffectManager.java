package org.ancestra.evolutive.spells;

import java.lang.reflect.Constructor;
import java.util.function.BiConsumer;

import org.ancestra.evolutive.new_database.data.spells.XmlSpellEffect;
import org.ancestra.evolutive.newfight.Buff;
import org.ancestra.evolutive.newfight.BuffManager;
import org.ancestra.evolutive.newfight.IFight;
import org.ancestra.evolutive.newfight.IFighter;
import org.ancestra.evolutive.newfight.ISpellEffect;
import org.ancestra.evolutive.spells.buffs.PassTurnBuff;
import org.ancestra.evolutive.spells.buffs.SacrificeBuff;
import org.ancestra.evolutive.spells.effects.RaulebaqueEffect;

public class SpellEffectManager {
	
	private static final ISpellEffect[] effectAppliers = new ISpellEffect[952];
	static {
		effectAppliers[4] = (f,c,e,ci) -> f.teleportFighter(c, ci);

		effectAppliers[140] = createBuffApplierEffect(PassTurnBuff.class);
		effectAppliers[141] = createApplyToAffectedTargets((f, t) -> f.setFighterLife(t, 0));// Tue la cible
		
		effectAppliers[765] = createBuffApplierEffect(SacrificeBuff.class);
		
		effectAppliers[784] = new RaulebaqueEffect();
	}
	
	public static void applySpellEffectToFight(IFight fight, IFighter caster, XmlSpellEffect effect, short cellId) {
		effectAppliers[effect.getId()].apply(fight, caster, effect, cellId);
	}
	
	private static <T extends Buff> ISpellEffect createBuffApplierEffect(Class<T> buffClass) {
		try {
            final Constructor<T> ctr = buffClass.getConstructor(IFighter.class, IFighter.class, byte.class);
            return (f,c,e,ci) -> {
                BuffManager buffMgr = f.getBuffManager();
                f.getAffectedFighters(e, ci).forEach(t -> {
                    try {
                        buffMgr.addBuff(ctr.newInstance(c, t, e.getDuration()));
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                });
            };
        } catch (NoSuchMethodException | SecurityException e2) {
            e2.printStackTrace();
            return null;
        }
	}
	
	private static ISpellEffect createApplyToAffectedTargets(BiConsumer<IFight, IFighter> applier) {
		return (f,c,e,ci) -> f.getAffectedFighters(e, ci).forEach(t -> applier.accept(f, t));
	}
}
