package org.ancestra.evolutive.spells.effects;

import java.util.HashMap;

import org.ancestra.evolutive.new_database.data.spells.XmlSpellEffect;
import org.ancestra.evolutive.newfight.IFight;
import org.ancestra.evolutive.newfight.IFighter;
import org.ancestra.evolutive.newfight.ISpellEffect;

public class RaulebaqueEffect implements ISpellEffect {
	private final HashMap<IFighter, Short> cells = new HashMap<>();
	
	public RaulebaqueEffect() {
		super();
	}

	@Override
	public void onFightStart(IFight fight) {
		fight.getFighters().forEach(f -> cells.put(f, f.getCellId()));
	}
	
	@Override
	public void onFightEnd(IFight fight) {
		fight.getFighters().forEach(cells::remove);
	}
	
	@Override
	public void apply(IFight fight, IFighter caster, XmlSpellEffect effect, short cellId) {
		fight.getAffectedFighters(effect, cellId).forEach(f -> fight.teleportFighter(f, cells.get(f)));
	}
}
