package org.ancestra.evolutive.spells.buffs;

import org.ancestra.evolutive.newfight.Buff;
import org.ancestra.evolutive.newfight.CastInfos;
import org.ancestra.evolutive.newfight.IFighter;

public class SacrificeBuff extends Buff {

	public SacrificeBuff(IFighter caster,IFighter target, byte duration) {
		super(caster, target, duration);
	}

	@Override
	protected boolean applyOnHit(CastInfos castInfo) {
		castInfo.target = caster;
		// Swap the fighters locations
		//short tId = castInfo.target.getCellId();
		//castInfo.target.teleport(caster.getCellId());
		//caster.teleport(tId);
		return true;// Stop the buff chain
	}
}
