package org.ancestra.evolutive.spells.buffs;

import org.ancestra.evolutive.newfight.Buff;
import org.ancestra.evolutive.newfight.CastInfos;
import org.ancestra.evolutive.newfight.IFighter;

public class SpellReflectBuff extends Buff {

	public SpellReflectBuff(IFighter caster, IFighter target, byte duration) {
		super(caster, target, duration);
	}

	@Override
	protected boolean applyOnHit(CastInfos castInfo) {
		IFighter tmp = castInfo.target;
		castInfo.target = castInfo.caster;
		castInfo.caster = tmp;
		return true;// Stop the buff chain, we changed the target
	}
}
