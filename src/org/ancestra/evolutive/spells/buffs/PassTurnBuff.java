package org.ancestra.evolutive.spells.buffs;

import org.ancestra.evolutive.newfight.Buff;
import org.ancestra.evolutive.newfight.IFighter;

public class PassTurnBuff extends Buff {

	public PassTurnBuff(IFighter caster, IFighter target, byte duration) {
		super(caster, target, duration);
	}

	@Override
	protected void applyOnTurnStart() {
		//target.endTurn();
	}
}
