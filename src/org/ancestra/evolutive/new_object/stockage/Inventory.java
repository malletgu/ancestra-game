package org.ancestra.evolutive.new_object.stockage;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.beans.Transient;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * Thread safe class for Inventory For Maps, equipped array is used for
 * ItemStack dropped on the ground For Players, it is used for shortcuts and
 * equipment For collector and exchange, it is null.
 * 
 * @author Deamon
 *
 */
@Entity
public class Inventory {
	private final AtomicReferenceArray<Item> equipped;
	private final List<ItemStack> bag = new LinkedList<>();// NEVER create a
															// setter for bag.
	private final AtomicLong kamas;

	public Inventory(int size, boolean enableKamas) {
		if (size > 0) {
			equipped = new AtomicReferenceArray<>(size);
		} else {
			equipped = null;
		}
		if (enableKamas) {
			kamas = new AtomicLong();
		} else {
			kamas = null;
		}
	}

    @Transient
	public boolean modifKamas(long kamas) {
		if(this.kamas==null) {
			return false;
		}
		long newKamas;
		long oldKamas;
		do {
			oldKamas = this.kamas.get();
			newKamas = oldKamas + kamas;
			if (newKamas < 0)
				return false;// check for overflows
		} while (!this.kamas.compareAndSet(oldKamas, newKamas));
		return true;
	}

	public long getKamas() {
		return this.kamas==null?0:kamas.get();
	}

    @Transient
	public Item getEquippedItem(int pos) {
		if (equipped == null || pos < 0 || pos > equipped.length())
			return null;
		return equipped.get(pos);
	}

    @Transient
	public void equip(Item it, int pos) {
		if (equipped == null || pos < 0 || pos > equipped.length()) {
			throw new IllegalAccessError(
					"Trying to equip an item but Inventory.equiped == null or invalid position ("
							+ pos + ")");
			// May be replaced by return false, and a error log to avoid
			// exception
		}
		Item old = equipped.getAndSet(pos, it);
		if (old == null) {
			return;
		}
		addItemBag(new ItemStack(old, 1));
	}

    @Transient
	public void takeOff(int pos) {
		equip(null, pos);
	}

    @Transient
	public void addItemBag(ItemStack itemStack) {
		synchronized (bag) {
			Optional<ItemStack> itm = bag.stream()
					.filter(is -> is.item.equals(itemStack.item)).findAny();
			// Merge stack
			if (itm.isPresent()) {
				itm.get().quantity += itemStack.quantity;
				return;
			}
			// No identical item
			bag.add(itemStack);
		}
	}

	/**
	 * Remove items from bag. Do not take off or remove equipped items.
	 * 
	 * @param itemStack
	 *            the item and quantity to remove
	 * @return true if success
	 */
    @Transient
	public boolean removeItemFromBag(ItemStack itemStack) {
		synchronized (bag) {
			ListIterator<ItemStack> lit = bag.listIterator();
			while (lit.hasNext()) {
				ItemStack is = lit.next();
				if (!is.item.equals(itemStack.item))
					continue; // Not the wanted item

				int newQuantity = is.quantity - itemStack.quantity;
				if (newQuantity < 0)
					return false; // not enough item

				if (newQuantity == 0)
					lit.remove(); // If 0 item left, remove the itemStack
				else
					is.quantity = newQuantity; // update quantity
				return true;
			}
			return false;// Not found
		}
	}

    @Embeddable
	public static class ItemStack {
		final Item item;
		int quantity; // not public, should be used by Inventory class only

		public ItemStack(Item item, int quantity) {
			this.item = item;
			this.quantity = quantity;
		}
	}

}

@Entity
class Item {
	final int template = 0; // replace int by "ItemTemplate"
	final int stats = 0; // replace int by "Stats" or ItemStats

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Item)) {
			return false;
		}
		Item itm = (Item) obj;
		return Objects.equals(template, itm.template)
				&& Objects.equals(stats, itm.stats); // OBjects.equals to avoid
														// NullPointerException
														// if no stats
	}
}