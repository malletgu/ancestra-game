package org.ancestra.evolutive.new_object.stockage;

import org.ancestra.evolutive.object.Object;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.beans.Transient;
import java.util.*;

/**
 * Created by guillaume  on 01/10/14.
 * New class to manage inventory
 */
@Entity
public class BasicInventory {
    @Embeddable
    private static class Container {
        Object object;
        int quantity;

        public Container(Object object, int quantity) {
            this.object = object;
            this.quantity = quantity;
        }
    }

    private ArrayList<Container> objects = new ArrayList<>();
    private int kamas = 0;

    /**
     * Remove one quantity of the specified object and destruct it if needed
     * @param object object to decrease quantity
     * @return true if success,false if fail(non existing object)
     */
    @Transient
    public boolean removeObject(Object object){
        return removeObject(object,1);
    }

    /**
     * Remove the specified quantity for the object and destruct it if needed
     * @param object object to decrease quantity
     * @param quantity quantity to remove
     * @return true if success,false if fail(non existing object)
     */
    @Transient
    public boolean removeObject(Object object,int quantity){
        Container entry = get(object);//La complexite est de O(N) donc on prend une variable
        if(entry.quantity < quantity)
            return false;
        if(entry.quantity == quantity)
            this.objects.remove(get(object));
        else
            entry.quantity -= quantity;
        return true;
    }

    /**
     * Add an object at the end of the list with a quantity of one or increase quantity if object exist
     * @param object object to add
     */
    @Transient
    public void addObject(Object object){
        addObject(object, 1);
    }

    /**
     * Add an object at the end of the list with the specified quantity of one or increase quantity if object exist
     * @param object object to add
     * @param quantity quantity to add
     */
    @Transient
    public void addObject(Object object,int quantity){
        addObjectWithSpecifiedIndex(object,quantity,-1);
    }

    @Transient
    public void addObjectWithSpecifiedIndex(Object object,int quantity,int index){
        Container entry = get(object);//La complexite est de O(N) donc on prend une variable
        if(index < 0 || index >= this.objects.size())
            index = -1;
        if(entry == null)
            if(index == -1)
                this.objects.add(new Container(object,quantity));
            else
                this.objects.add(index,new Container(object,quantity));
        else
            entry.quantity += quantity;
    }

    /**
     * @return number of objects in list
     */
    @Transient
    public int getObjectCount(){
        return this.objects.size();
    }

    /**
     * Replace old object by new object
     * @param oldPlace old position of the object
     * @param newPlace position wanted
     */
    @Transient
    public void replace(int oldPlace, int newPlace){
        if(oldPlace >= objects.size() || oldPlace < 0 || newPlace < 0) return;
        if(newPlace == oldPlace) return;
        if(newPlace < objects.size()) {
            Container temp = this.objects.get(newPlace);
            this.objects.add(newPlace+1, this.objects.get(oldPlace));
            this.objects.remove(newPlace);
            this.objects.add(oldPlace+1,temp);
        }
        else {
            this.objects.add(this.objects.get(oldPlace));
        }
        this.objects.remove(oldPlace);
    }

    @Transient
    public boolean contains(Object object){
        return get(object)!=null;
    }

    @Transient
    public int getQuantity(Object object){
        Container entry = get(object);
        return entry==null?-1:entry.quantity;
    }

    public int getKamas() {
        return kamas;
    }

    public void setKamas(int kamas) {
        this.kamas = kamas;
    }

    @Transient
    public void addKamas(int kamas){
        this.kamas += kamas;
    }

    @Transient
    public boolean removeKamas(int kamas){
        if (this.kamas-kamas<0)
            return false;
        this.kamas-=kamas;
        return true;
    }

    @Transient
    private Container get(Object object){
        for(Container entry  : objects){
            if(entry.object.equals(object))
                return entry;
        }
        return null;
    }
}
