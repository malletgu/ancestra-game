package org.ancestra.evolutive.new_object;

import org.ancestra.evolutive.object.ObjectType;

public interface Arme {

    public ObjectType getType();
}
